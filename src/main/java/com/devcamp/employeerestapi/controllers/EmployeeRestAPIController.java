package com.devcamp.employeerestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.employeerestapi.models.Employee;

@RestController
@RequestMapping("/")
@CrossOrigin
public class EmployeeRestAPIController {
    @GetMapping("/employee")
    public ArrayList<Employee> getEmployees() {
        ArrayList<Employee> employees = new ArrayList<Employee>();
        Employee employee1 = new Employee(1, "An", "Le", 5000);
        Employee employee2 = new Employee(2, "Minh", "Hoang", 6000);
        Employee employee3 = new Employee(3, "Thi", "Hoang", 3000);

        System.out.println(employee1.toString());
        System.out.println(employee2.toString());
        System.out.println(employee3.toString());

        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);
        
        return employees;  
    }
}
